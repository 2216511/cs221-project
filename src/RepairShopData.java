// Data Access Class

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RepairShopData {
    private static Connection connection;

    private RepairShopData() {
        // private constructor
    }

    /**
     * Method for setting a connection to the database
     */
    public static void setConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/repairshop","root", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to close the connection
     */
    public static void closeConnection() {
        Connection con = null;
        try {
            if (con != null)
                con.close();
                System.out.println("Connection closed...");
                System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to add customer to the database
     */
    public static void addCustomer(Customer customer) {
        int cid = 0;

        PreparedStatement ps;
        String psq1 = "INSERT INTO `customer` (`cid`, `cname`, `cmobilenum`, `cemail`, `caddress`) VALUES" +
                " (?, ?, ?, ?, ?)";
        String psq2 = "SELECT cid FROM repairshop.customer ORDER BY cid DESC LIMIT 1;";

        try {
            ps = connection.prepareStatement(psq2, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while(rs.next()) {
                cid = Integer.parseInt(rs.getString(1));
            }
            cid = cid + 1;
            customer.setCID(Integer.toString(cid));
            String customerID = String.valueOf(cid);

            ps = connection.prepareStatement(psq1);
            ps.setString(1, customerID);
            ps.setString(2, customer.getCName());
            ps.setString(3, customer.getCMobNum());
            ps.setString(4, customer.getCEmail());
            ps.setString(5, customer.getCAddress());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to add employee to the database
     */
    public static void addEmployee(Employee employee) {
        PreparedStatement ps;
        String psq = "INSERT INTO `employee` (`emid`, `emname`, `emobilenum`, `ememail`, `job`) VALUES" +
                " (?, ?, ?, ?, ?)";

        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, employee.getEID());
            ps.setString(2, employee.getEName());
            ps.setString(3, employee.getEMobNum());
            ps.setString(4, employee.getEEmail());
            ps.setString(5, employee.getEJob());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to add inventory item to the database
     */
    public static void addInventory(Inventory inventory){
        PreparedStatement ps;
        String psq = "INSERT INTO `inventory` (`partid`, `partname`, `partcost`, `partsellprice`, `quantity`) VALUES" +
                " (?, ?, ?, ?, ?)";

        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, inventory.getPID());
            ps.setString(2, inventory.getPName());
            ps.setString(3, String.valueOf(inventory.getPCost()));
            ps.setString(4, String.valueOf(inventory.getPSellPrice()));
            ps.setString(5, String.valueOf(inventory.getQuantity()));
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to add request to the database
     */
    public static void addRepairDetails(RepairDetails repDetails) {
        int requestid = 0;

        PreparedStatement ps;
        String psq1 = "INSERT INTO `repairdetails` (`requestid`, `cid`, `emid`, `daterequested`, `phonemodel`," +
                " `estimatedcompletion`, `totalamount`, `requireddownpayment`, `status`) VALUES" +
                " (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String psq2 = "SELECT requestid FROM repairshop.repairdetails ORDER BY requestid DESC LIMIT 1;";

        try {
            ps = connection.prepareStatement(psq2, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while(rs.next()) {
                requestid = Integer.parseInt(rs.getString(1));
            }
            requestid = requestid + 1;
            repDetails.setRequestID(Integer.toString(requestid));

            ps = connection.prepareStatement(psq1);
            ps.setString(1, repDetails.getRequestID());
            ps.setString(2, repDetails.getCID());
            ps.setString(3, repDetails.getEID());
            ps.setString(4, repDetails.getDateRequested());
            ps.setString(5, repDetails.getPhoneModel());
            ps.setString(6, repDetails.getEstimatedCompletion());
            ps.setString(7, String.valueOf(repDetails.getTotal()));
            ps.setString(8, String.valueOf(repDetails.getRequiredDP()));
            ps.setString(9, repDetails.getStatus());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateRepairDetails(String rID) {
        PreparedStatement ps;
        String psq2 = "UPDATE repairdetails SET totalamount = ?, requireddownpayment = ? " +
                "  WHERE requestid = ?;";
        String totalAmount = String.valueOf(RepairShopData.computeTotalAmount(rID));

        try {
            ps = connection.prepareStatement(psq2, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, totalAmount);
            ps.setString(2, String.valueOf(RepairShopData.computeDownPayment(Double.parseDouble(totalAmount))));
            ps.setString(3, rID);
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double computeDownPayment(double totalAmount){
        return totalAmount * 0.05;
    }

    public static String getRID(){
        int rID = 0;
        PreparedStatement ps;
        String psq = "SELECT requestid FROM repairshop.repairdetails ORDER BY requestid DESC LIMIT 1;";

        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                rID = Integer.parseInt(rs.getString(1));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return String.valueOf(rID);
    }

    public static String getTypeRepID(String typeRepName){
        PreparedStatement ps;
        String typeRepID = "";
        String psq = "SELECT typeid FROM repairshop.typeofrepair WHERE damageinfo = ?";

        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, typeRepName);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while(rs.next()) {
                typeRepID = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return typeRepID;
    }

    public static String getRepInvID(String repInvName){
        PreparedStatement ps;
        String typeRepID = "";
        String psq = "SELECT partid FROM repairshop.inventory WHERE partname = ?";

        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, repInvName);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while(rs.next()) {
                typeRepID = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return typeRepID;
    }

    /**
     * Method to get the customer ID using the specified customer's name
     */
    public static String getCID(String customerName) {
        PreparedStatement ps;
        String cid = "";
        String psq = "SELECT cid FROM repairshop.customer WHERE cname = ?";

        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, customerName);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while(rs.next()) {
                cid = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cid;
    }

    /**
     * Method to get the employee ID using the specified employee's name
     */
    public static String getEID(String employeeName) {
        PreparedStatement ps;
        String eid = "";
        String psq = "SELECT emid FROM repairshop.employee WHERE emname = ?";

        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, employeeName);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while(rs.next()) {
                eid = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return eid;
    }

    public static void addTypeRep(String rID, String typeID) {
        PreparedStatement ps;
        String psq = "INSERT INTO `typerep` (`requestid`, `typeid`) VALUES" +
                " (?, ?)";

        try {
            ps = connection.prepareStatement(psq);
            ps.setString(1, rID);
            ps.setString(2, typeID);
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addRepInv(String rID, String pID) {
        PreparedStatement ps;
        String psq = "INSERT INTO `repinv` (`requestid`, `partid`) VALUES" +
                " (?, ?)";

        try {
            ps = connection.prepareStatement(psq);
            ps.setString(1, rID);
            ps.setString(2, pID);
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that computes the total amount fee of the specified request ID
     */
    public static double computeTotalAmount(String requestID) {
        double partSellPrice = computePartSellPrice(requestID);
        double servicePrice = computeServicePrice(requestID);
        return partSellPrice + servicePrice;
    }

    public static double computePartSellPrice(String requestID) {
        PreparedStatement ps;
        double partSellPrice = 0.0;
        String psq2 = "SELECT partsellprice FROM repairshop.repairdetails NATURAL JOIN repairshop.repinv" +
                " NATURAL JOIN repairshop.inventory WHERE requestid = ?";
        try {
            ps = connection.prepareStatement(psq2);
            ps.setString(1, requestID);
            ResultSet rs = ps.executeQuery();
            //rs.beforeFirst();
            while (rs.next()) {
                partSellPrice += Double.parseDouble(rs.getString(1));
            }
            rs.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return partSellPrice;
    }

    public static double computeServicePrice(String requestID){
        PreparedStatement ps;
        double servicePrice = 0.0;
        String psq1 = "SELECT serviceprice FROM repairshop.repairdetails NATURAL JOIN repairshop.typerep" +
                " NATURAL JOIN repairshop.typeofrepair WHERE requestid = ?";

        try {
            ps = connection.prepareStatement(psq1, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, requestID);
            ResultSet rs = ps.executeQuery();
            //rs.beforeFirst();
            while (rs.next()) {
                servicePrice += Double.parseDouble(rs.getString(1));
            }
            rs.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return servicePrice;
    }

    public static void decrementInventoryItem(String repInvID){
        PreparedStatement ps;
        String psq2 = "UPDATE inventory SET quantity = ? " +
                "  WHERE partid = ?;";
        String newQuantity = String.valueOf(RepairShopData.getQuantity(repInvID) - 1);

        try {
            ps = connection.prepareStatement(psq2, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, newQuantity);
            ps.setString(2, repInvID);
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getQuantity(String repInvID){
        PreparedStatement ps;
        int quantity = 0;
        String psq = "SELECT quantity FROM repairshop.inventory WHERE partid = ?";

        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, repInvID);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while(rs.next()) {
                quantity = Integer.parseInt(rs.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return quantity;
    }

    /**
     * Method that returns the name of the customers from the customer table
     */
    public static String[] getCustomerNameList() throws Exception {
        ArrayList<String> cidList = new ArrayList<>();
        String sq = "SELECT cname FROM customer";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet rs = st.executeQuery(sq);

        while(rs.next()){
            cidList.add(rs.getString(1));
        }
        rs.close();
        String[] stringList = new String[cidList.size()];
        for (int i = 0; i < cidList.size(); i++)
            stringList[i] = cidList.get(i);
        return stringList;
    }

    /**
     * Method that returns the name of the employees from the employee table
     */
    public static String[] getEmployeeNameList() throws Exception {
        String sq = "SELECT emname FROM employee";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet resultSet = st.executeQuery(sq);
        ArrayList<String> eidList = new ArrayList<>();

        while(resultSet.next()){
            eidList.add(resultSet.getString(1));
        }
        resultSet.close();
        String[] stringEIDList = new String[eidList.size()];
        for (int i = 0; i < eidList.size(); i++)
            stringEIDList[i] = eidList.get(i);
        return stringEIDList;
    }

    /**
     * Method that returns the data of the customer table
     */
    public static String[] getTypeOfRepairList() throws Exception {
        String sq = "SELECT damageinfo FROM typeofrepair";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet resultSet = st.executeQuery(sq);
        ArrayList<String> typeOfRepairList = new ArrayList<>();

        while(resultSet.next()){
            typeOfRepairList.add(resultSet.getString(1));
        }
        resultSet.close();
        String[] stringEIDList = new String[typeOfRepairList.size()];
        for (int i = 0; i < typeOfRepairList.size(); i++)
            stringEIDList[i] = typeOfRepairList.get(i);
        return stringEIDList;
    }

    public static String[] getInventoryList() throws Exception {
        String sq = "SELECT partname FROM inventory";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet resultSet = st.executeQuery(sq);
        ArrayList<String> invList = new ArrayList<>();

        while(resultSet.next()){
            invList.add(resultSet.getString(1));
        }
        resultSet.close();
        String[] inventoryList = new String[invList.size()];
        for (int i = 0; i < invList.size(); i++)
            inventoryList[i] = invList.get(i);
        return inventoryList;
    }

    public static ArrayList<Customer> getCustomers() throws Exception {
        ArrayList<Customer> listOfCustomers = new ArrayList<>();
        String sq = "SELECT * FROM customer";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet rs = st.executeQuery(sq);

        while(rs.next()) {
            Customer cus = new Customer(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
            listOfCustomers.add(cus); // populates the ArrayList
        }

        rs.close();
        return listOfCustomers;
    }

    /**
     * Method that returns the data of the employee table
     */
    public static ArrayList<Employee> getEmployees() throws Exception {
        ArrayList<Employee> listOfEmployees = new ArrayList<>();
        String sq = "SELECT * FROM employee";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet rs = st.executeQuery(sq);

        while(rs.next()) {
            Employee em = new Employee(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
            listOfEmployees.add(em); // populates the ArrayList
        }

        rs.close();
        return listOfEmployees;
    }

    /**
     * Method that returns the data of the inventory table
     */
    public static ArrayList<Inventory> getInventory() throws Exception {
        ArrayList<Inventory> listOfInventory = new ArrayList<>();
        String sq = "SELECT * FROM inventory";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet rs = st.executeQuery(sq);

        while(rs.next()) {
            Inventory inv = new Inventory(rs.getString(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4), rs.getInt(5));
            listOfInventory.add(inv); // populates the ArrayList
        }

        rs.close();
        return listOfInventory;
    }

    /**
     * Method that returns the data of the payment table
     */
    public static ArrayList<Payment> getPayments() throws Exception {
        ArrayList<Payment> listOfPayments = new ArrayList<>();
        String sq = "SELECT * FROM payment";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet rs = st.executeQuery(sq);

        while(rs.next()) {
            Payment pay = new Payment(rs.getString(1), rs.getString(2), rs.getDouble(3), rs.getString(4), rs.getDouble(5), rs.getString(6));
            listOfPayments.add(pay); // populates the ArrayList
        }

        rs.close();
        return listOfPayments;
    }

    /**
     * Method that returns the data of the repairdetails table
     */
    public static ArrayList<RepairDetails> getRepairDetails() throws Exception {
        ArrayList<RepairDetails> listOfRepairDetails = new ArrayList<>();
        String sq = "SELECT * FROM repairdetails";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet rs = st.executeQuery(sq);

        while(rs.next()) {
            RepairDetails rd = new RepairDetails(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDouble(7), rs.getDouble(8), rs.getString(9));
            listOfRepairDetails.add(rd); // populates the ArrayList
        }

        rs.close();
        return listOfRepairDetails;
    }

    /**
     * Method that returns the data of the repinv table
     */
    public static ArrayList<RepInv> getRepInv() throws Exception {
        ArrayList<RepInv> listOfRepInv = new ArrayList<>();
        String sq = "SELECT * FROM repinv";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet rs = st.executeQuery(sq);

        while(rs.next()) {
            RepInv ri = new RepInv(rs.getString(1), rs.getString(2));
            listOfRepInv.add(ri); // populates the ArrayList
        }

        rs.close();
        return listOfRepInv;
    }

    /**
     * Method that returns the data of the typefrepair table
     */
    public static ArrayList<RepairType> getRepairTypes() throws Exception {
        ArrayList<RepairType> listOfRepType = new ArrayList<>();
        String sq = "SELECT * FROM typeofrepair";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet rs = st.executeQuery(sq);

        while(rs.next()) {
            RepairType rt = new RepairType(rs.getString(1), rs.getString(2), rs.getDouble(3));
            listOfRepType.add(rt); // populates the ArrayList
        }

        rs.close();
        return listOfRepType;
    }

    /**
     * Method that returns the data of the typerep table
     */
    public static ArrayList<TypeRep> getTypeRep() throws Exception {
        ArrayList<TypeRep> listOfTypeRep = new ArrayList<>();
        String sq = "SELECT * FROM typerep";
        Statement st = connection.createStatement(ResultSet.CONCUR_UPDATABLE, ResultSet.TYPE_SCROLL_SENSITIVE);
        ResultSet rs = st.executeQuery(sq);

        while(rs.next()) {
            TypeRep tr = new TypeRep(rs.getString(1), rs.getString(2));
            listOfTypeRep.add(tr); // populates the ArrayList
        }

        rs.close();
        return listOfTypeRep;
    }

    /**
     * Method to search in customer table with the specified input keyword
     */
    public static ArrayList<Customer> findCustomer(String input) {
        ArrayList<Customer> result = new ArrayList<>();
        Customer customer = null;
        PreparedStatement ps;
        input = '%' + input + '%';
        String psq = "SELECT * FROM customer WHERE cname LIKE ? OR cmobilenum LIKE ? OR cemail LIKE ? OR caddress LIKE ? ORDER BY cname";
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            ps.setString(1, input);
            ps.setString(2, input);
            ps.setString(3, input);
            ps.setString(4, input);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                customer = new Customer(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                result.add(customer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to search in employee table with the specified input keyword
     */
    public static ArrayList<Employee> findEmployee(String input) {
        ArrayList<Employee> result = new ArrayList<>();
        Employee employee;
        PreparedStatement ps;
        String psq = "SELECT * FROM employee WHERE emname LIKE ? OR emobilenum LIKE ? OR ememail LIKE ? ORDER BY emname";
        input = '%' + input + '%';

        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input);
            ps.setString(2, input);
            ps.setString(3, input);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                employee = new Employee(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                result.add(employee);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to search in inventory table with the specified input keyword
     */
    public static ArrayList<Inventory> findInventory(String input) {
        ArrayList<Inventory> result = new ArrayList<>();
        Inventory inventory = null;
        PreparedStatement ps;
        String psq = "SELECT * FROM inventory WHERE partname LIKE ? ORDER BY partname";
        input = '%' + input + '%';
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                inventory = new Inventory(rs.getString(1), rs.getString(2), Double.parseDouble(rs.getString(3)), Double.parseDouble(rs.getString(4)), Integer.parseInt(rs.getString(5)));
                result.add(inventory);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to search in payment table with the specified input keyword
     */
    public static ArrayList<Payment> findPayment(String input){
        ArrayList<Payment> result = new ArrayList<>();
        Payment payment = null;
        PreparedStatement ps;
        String psq = "SELECT * FROM payment WHERE payid LIKE ? OR datepaid LIKE ? OR amountPaid LIKE ? OR " +
                "typeofpayment LIKE ? OR amountremaining LIKE ? OR requestid LIKE ? ORDER BY payid";
        input = '%' + input + '%';
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input);
            ps.setString(2, input);
            ps.setString(3, input);
            ps.setString(4, input);
            ps.setString(5, input);
            ps.setString(6, input);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                payment = new Payment(rs.getString(1), rs.getString(2), rs.getDouble(3), rs.getString(4), rs.getDouble(5), rs.getString(6));
                result.add(payment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to search in repairdetails table with the specified input keyword
     */
    public static ArrayList<RepairDetails> findRepairDetails(String input){
        ArrayList<RepairDetails> result = new ArrayList<>();
        RepairDetails repairDetails;
        PreparedStatement ps;
        String psq = "SELECT * FROM repairdetails WHERE requestid LIKE ? OR cid LIKE ? OR emid LIKE ? OR " +
                "daterequested LIKE ? OR phonemodel LIKE ? OR estimatedcompletion LIKE ? OR totalamount LIKE ? " +
                "OR requireddownpayment LIKE ? OR status LIKE ? ORDER BY requestid";
        input = '%' + input + '%';
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input);
            ps.setString(2, input);
            ps.setString(3, input);
            ps.setString(4, input);
            ps.setString(5, input);
            ps.setString(6, input);
            ps.setString(7, input);
            ps.setString(8, input);
            ps.setString(9, input);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                repairDetails = new RepairDetails(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDouble(7), rs.getDouble(8), rs.getString(9));
                result.add(repairDetails);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to filter customer by address
     */
    public static ArrayList<Customer> filterCustomerByAddress(String input) throws SQLException {
        ArrayList<Customer> result = new ArrayList<>();
        Customer customer;
        PreparedStatement ps;
        input = '%' + input + '%';
        String psq = "SELECT * FROM customer WHERE caddress LIKE ? ORDER BY caddress";
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                customer = new Customer(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                result.add(customer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to filter employee by job
     */
    public static ArrayList<Employee> filterEmployeeByJob(String input) {
        ArrayList<Employee> result = new ArrayList<>();
        Employee employee = null;
        PreparedStatement ps;
        String psq = "SELECT * FROM employee WHERE job = ? ORDER BY job";
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                employee = new Employee(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                result.add(employee);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to filter inventory by part name
     * Inputs accepted are: Iphone, Galaxy, Tab A, Note, J7, Note, V12,G5, Pixel, Five, 9 Pro
     */
    public static ArrayList<Inventory> filterInventoryByPartName(String input) {
        ArrayList<Inventory> result = new ArrayList<>();
        Inventory inventory = null;
        PreparedStatement ps;
        String psq = "SELECT * FROM inventory WHERE partname LIKE ? ORDER BY partname";
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input + '%');
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                inventory = new Inventory(rs.getString(1), rs.getString(2), Double.parseDouble(rs.getString(3)), Double.parseDouble(rs.getString(4)), Integer.parseInt(rs.getString(5)));
                result.add(inventory);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to filter payment by month and year
     */
    public static ArrayList<Payment> filterPaymentByMonthAndYear(String month, String year) {
        ArrayList<Payment> result = new ArrayList<>();
        Payment payment = null;
        PreparedStatement ps;
        String psq = "SELECT * FROM payment WHERE datepaid LIKE ? ORDER BY datepaid DESC";
        String input;
        if (month.length() == 1) {
            input = year + "-0" + month;
        } else {
            input = year + '-' + month;
        }
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input + '%');
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                payment = new Payment(rs.getString(1), rs.getString(2), Double.parseDouble(rs.getString(3)), rs.getString(4), Double.parseDouble(rs.getString(5)), rs.getString(6));
                result.add(payment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to filter payment by type of payment
     * Inputs accepted are: Bank Transfer, Cash, E-wallet, Credit, Bank Transfer
     */
    public static ArrayList<Payment> filterPaymentByTypeOfPayment(String input) {
        ArrayList<Payment> result = new ArrayList<>();
        Payment payment = null;
        PreparedStatement ps;
        String psq = "SELECT * FROM payment WHERE typeofpayment = ? ORDER BY datepaid DESC";
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                payment = new Payment(rs.getString(1), rs.getString(2), Double.parseDouble(rs.getString(3)), rs.getString(4), Double.parseDouble(rs.getString(5)), rs.getString(6));
                result.add(payment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to filter repair details by status
     * Inputs accepted are: RETURNED, ONGOING, PENDING, CANCELED, FINISHED
     */
    public static ArrayList<RepairDetails> filterRepairDetailsByStatus(String input) {
        ArrayList<RepairDetails> result = new ArrayList<>();
        RepairDetails repairDetails;
        PreparedStatement ps;
        String psq = "SELECT * FROM repairdetails WHERE status = ? ORDER BY requestid";
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input);
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                if (rs.getString(8) == null){
                    repairDetails = new RepairDetails(rs.getString(1), rs.getString(2),
                            rs.getString(3), rs.getString(4), rs.getString(5),
                            rs.getString(6), Double.parseDouble(rs.getString(7)),
                            0, rs.getString(9));
                } else {
                    repairDetails = new RepairDetails(rs.getString(1), rs.getString(2),
                            rs.getString(3), rs.getString(4), rs.getString(5),
                            rs.getString(6), Double.parseDouble(rs.getString(7)),
                            Double.parseDouble(rs.getString(8)), rs.getString(9));
                }
                result.add(repairDetails);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to filter repair details by month and year requested
     */
    public static ArrayList<RepairDetails> filterRepairDetailsByMonthAndYear(String month, String year) {
        ArrayList<RepairDetails> result = new ArrayList<>();
        RepairDetails repairDetails = null;
        PreparedStatement ps;
        String psq = "SELECT * FROM repairdetails WHERE daterequested LIKE ? ORDER BY daterequested DESC";
        String input;
        if (month.length() == 1) {
            input = year + "-0" + month;
        } else {
            input = year + '-' + month;
        }
        try {
            ps = connection.prepareStatement(psq, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, input + '%');
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            while (rs.next()) {
                repairDetails = new RepairDetails(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), Double.parseDouble(rs.getString(7)), Double.parseDouble(rs.getString(8)), rs.getString(9));
                result.add(repairDetails);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
