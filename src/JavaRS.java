// Main class

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Objects;
public class JavaRS {

    /**
     * Main method
     */
    public static void main(String[] args) {
        RepairShopData.setConnection(); // connect to database
        showMenu();
    }

    /**
     * Method for showing Menu options
     */
    public static void showMenu() {
        JFrame menuFrame;
        JButton showTables;
        JButton addOperations;
        JButton quitButton;

        menuFrame = new JFrame("Repair Shop");
        JPanel panel = new JPanel();
        menuFrame.getContentPane().setLayout(new BorderLayout());
        menuFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        menuFrame.setResizable(false);
        menuFrame.setSize(500, 360);
        menuFrame.setLocationRelativeTo(null);
        menuFrame.add(panel);
        panel.setLayout(null);

        JLabel menuLabel = new JLabel("WiFix Repair Shop");
        menuLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        menuLabel.setBounds(155, 40, 400, 25);
        panel.add(menuLabel);

        showTables = new JButton("Show Tables");
        showTables.setBounds(90, 90, 300, 30);
        showTables.addActionListener(e -> {
            menuFrame.dispose();
            showTablesMenu();
        });
        panel.add(showTables);

        addOperations = new JButton("Add Operations");
        addOperations.setBounds(90, 140, 300, 30);
        addOperations.addActionListener(e -> {
            menuFrame.dispose();
            addOperations();
        });
        panel.add(addOperations);

        quitButton = new JButton("Quit");
        quitButton.setBounds(90, 190, 300, 30);
        quitButton.addActionListener(e -> {
            RepairShopData.closeConnection();
            System.exit(0);
        });
        panel.add(quitButton);
        menuFrame.setVisible(true);
    }

    /**
     * Method for showing list of tables
     */
    public static void showTablesMenu() {
        JFrame showTablesFrame;
        JButton customer;
        JButton employee;
        JButton inventory;
        JButton payment;
        JButton repairDetails;
        JButton repairType;
        JButton repInv;
        JButton typeRep;
        JButton quitButton;

        showTablesFrame = new JFrame("WiFix Repair Shop");
        JPanel panel = new JPanel();
        showTablesFrame.getContentPane().setLayout(new BorderLayout());
        showTablesFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        showTablesFrame.setResizable(false);
        showTablesFrame.setSize(500, 630);
        showTablesFrame.setLocationRelativeTo(null);
        showTablesFrame.add(panel);
        panel.setLayout(null);

        JLabel menuLabel = new JLabel("Show Tables");
        menuLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        menuLabel.setBounds(180, 40, 400, 25);
        panel.add(menuLabel);

        customer = new JButton("Customer");
        customer.setBounds(90, 90, 300, 30);
        customer.addActionListener(e -> {
            showTablesFrame.dispose();
            try {
                printCustomerTable(RepairShopData.getCustomers());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(customer);

        employee = new JButton("Employee");
        employee.setBounds(90, 140, 300, 30);
        employee.addActionListener(e -> {
            showTablesFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.getEmployees());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(employee);

        inventory = new JButton("Inventory");
        inventory.setBounds(90, 190, 300, 30);
        inventory.addActionListener(e -> {
            showTablesFrame.dispose();
            try {
                printInventoryTable(RepairShopData.getInventory());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(inventory);


        payment = new JButton("Payment");
        payment.setBounds(90, 240, 300, 30);
        payment.addActionListener(e -> {
            showTablesFrame.dispose();
            try {
                printPaymentTable(RepairShopData.getPayments());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(payment);

        repairDetails = new JButton("Repair Details");
        repairDetails.setBounds(90, 290, 300, 30);
        repairDetails.addActionListener(e -> {
            showTablesFrame.dispose();
            try {
                printRepairDetailsTable(RepairShopData.getRepairDetails());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(repairDetails);

        repInv = new JButton("RepInv");
        repInv.setBounds(90, 340, 300, 30);
        repInv.addActionListener(e -> {
            showTablesFrame.dispose();
            try {
                printRepInvTable();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(repInv);

        repairType = new JButton("Type of Repair");
        repairType.setBounds(90, 390, 300, 30);
        repairType.addActionListener(e -> {
            showTablesFrame.dispose();
            try {
                printRepairTypeTable();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(repairType);

        typeRep = new JButton("TypeRep");
        typeRep.setBounds(90, 440, 300, 30);
        typeRep.addActionListener(e -> {
            showTablesFrame.dispose();
            try {
                printTypeRepTable();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(typeRep);

        quitButton = new JButton("Back to Main Menu");
        quitButton.setBounds(90, 490, 300, 30);
        quitButton.addActionListener(e -> {
            showTablesFrame.dispose();
            showMenu();
        });
        panel.add(quitButton);

        showTablesFrame.setVisible(true);
    }

    /**
     * Method for add operations option
     */
    public static void addOperations() {
        JFrame addOperationsFrame;
        JButton addCustomer;
        JButton addEmployee;
        JButton addRepairDetails;
        JButton addInventory;
        JButton quitButton;

        addOperationsFrame = new JFrame("Repair Shop");
        JPanel panel = new JPanel();
        addOperationsFrame.getContentPane().setLayout(new BorderLayout());
        addOperationsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addOperationsFrame.setResizable(false);
        addOperationsFrame.setSize(500, 430);
        addOperationsFrame.setLocationRelativeTo(null);
        addOperationsFrame.add(panel);
        panel.setLayout(null);

        JLabel menuLabel = new JLabel("Add Operations");
        menuLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        menuLabel.setBounds(165, 40, 400, 25);
        panel.add(menuLabel);

        addCustomer = new JButton("Add Customer");
        addCustomer.setBounds(90, 90, 300, 30);
        addCustomer.addActionListener(e -> {
            addOperationsFrame.dispose();
            try {
                addCustomer();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(addCustomer);

        addEmployee = new JButton("Add Employee");
        addEmployee.setBounds(90, 140, 300, 30);
        addEmployee.addActionListener(e -> {
            addOperationsFrame.dispose();
            try {
                addEmployee();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(addEmployee);

        addRepairDetails = new JButton("Add Repair Details");
        addRepairDetails.setBounds(90, 190, 300, 30);
        addRepairDetails.addActionListener(e -> {
            addOperationsFrame.dispose();
            try {
                addRepairDetails();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(addRepairDetails);

        addInventory = new JButton("Add Inventory");
        addInventory.setBounds(90, 240, 300, 30);
        addInventory.addActionListener(e -> {
            addOperationsFrame.dispose();
            try {
                addInventory();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        panel.add(addInventory);

        quitButton = new JButton("Go Back");
        quitButton.setBounds(90, 290, 300, 30);
        quitButton.addActionListener(e -> {
            addOperationsFrame.dispose();
            showMenu();
        });
        panel.add(quitButton);

        addOperationsFrame.setVisible(true);
    }

    /**
     * Method for printing customer table
     */
    public static void printCustomerTable(ArrayList<Customer> customerList){
        JFrame customerFrame;
        JButton quitButton;
        JTable customerTable;

        String[] headers = new String[]{"Customer ID", "Customer Name", "Customer Number", "Customer Email", "Customer Address"};
        String[][] twoDimensionalArray = new String[customerList.size()][5];

        for (int i = 0; i < customerList.size(); i++) {
            Customer customer = customerList.get(i);
            twoDimensionalArray[i][0] = customer.getCID();
            twoDimensionalArray[i][1] = customer.getCName();
            twoDimensionalArray[i][2] = customer.getCMobNum();
            twoDimensionalArray[i][3] = customer.getCEmail();
            twoDimensionalArray[i][4] = customer.getCAddress();
        }

        customerTable = new JTable(twoDimensionalArray, headers);
        customerTable.setBounds(30, 40, 500, 400);
        customerTable.setDefaultEditor(Object.class, null);
        JScrollPane scrollPane = new JScrollPane(customerTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        quitButton = new JButton("Go back");
        quitButton.setBounds(30, 10, 150, 25);
        customerFrame = new JFrame("Customer Table");
        customerFrame.setSize(600, 500);
        customerFrame.setLocationRelativeTo(null);
        customerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quitButton.addActionListener(e -> {
            customerFrame.dispose();
            showTablesMenu();
        });

        //GUI for Search
        JMenuBar viewCustomerMenuBar = new JMenuBar();
        JMenu filterAndSearch = new JMenu("Filter / Search List");
        JMenuItem filterCustomerByAddress = new JMenuItem("Filter Customer By Address");
        JMenuItem findCustomer = new JMenuItem("Search Customer");

        filterAndSearch.add(filterCustomerByAddress);
        filterAndSearch.add(findCustomer);

        viewCustomerMenuBar.add(filterAndSearch);

        filterCustomerByAddress.addActionListener(e -> {
            customerFrame.dispose();
            try {
                printCustomerTable(RepairShopData.filterCustomerByAddress(showInputDialog("Type address filter:")));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        findCustomer.addActionListener(e -> {
            customerFrame.dispose();
            try {
                printCustomerTable(RepairShopData.findCustomer(JOptionPane.showInputDialog("Search customer:")));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        //filterCustomerByAddress.addActionListener(RepairShopData.filterCustomerByAddress());

        customerFrame.add(scrollPane, BorderLayout.CENTER);
        customerFrame.add(quitButton, BorderLayout.SOUTH);
        customerFrame.add(viewCustomerMenuBar, BorderLayout.NORTH);
        customerFrame.setVisible(true);

    }

    /**
     * Method for printing employee table
     */
    public static void printEmployeeTable(ArrayList<Employee> employeeList){
        JFrame employeeFrame;
        JButton quitButton;
        JTable employeeTable;

        String[] headers = new String[]{"Employee ID", "Employee Name", "Employee Number", "Employee Email", "Employee Address"};
        String[][] twoDimensionalArray = new String[employeeList.size()][5];

        for (int i = 0; i < employeeList.size(); i++) {
            Employee employee = employeeList.get(i);
            twoDimensionalArray[i][0] = employee.getEID();
            twoDimensionalArray[i][1] = employee.getEName();
            twoDimensionalArray[i][2] = employee.getEMobNum();
            twoDimensionalArray[i][3] = employee.getEEmail();
            twoDimensionalArray[i][4] = employee.getEJob();
        }

        employeeTable = new JTable(twoDimensionalArray, headers);
        employeeTable.setBounds(30, 40, 500, 400);
        employeeTable.setDefaultEditor(Object.class, null);
        JScrollPane scrollPane = new JScrollPane(employeeTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        quitButton = new JButton("Go back");
        quitButton.setBounds(30, 10, 150, 25);
        employeeFrame = new JFrame("Employee Table");
        employeeFrame.setSize(600, 500);
        employeeFrame.setLocationRelativeTo(null);
        employeeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quitButton.addActionListener(e -> {
            employeeFrame.dispose();
            showTablesMenu();
        });

        //GUI for Search
        JMenuBar viewEmployeeMenuBar = new JMenuBar();
        JMenu filterAndSearch = new JMenu("Filter / Search List");
        JMenu filterEmployeeByByJob = new JMenu("Filter Employee By Job");
        JMenuItem accountantCashier = new JMenuItem("Accountant/Cashier");
        JMenuItem clientServiceExecutive = new JMenuItem("Client Service Executive");
        JMenuItem frontDeskPersonnel = new JMenuItem("Front Desk Personnel");
        JMenuItem headTechnicalServices = new JMenuItem("Head, Technical Services");
        JMenuItem inspector = new JMenuItem("Inspector");
        JMenuItem maintenanceTech = new JMenuItem("Maintenance Technician");
        JMenuItem phoneBuildTechnician = new JMenuItem("Phone-Build Technician");
        JMenuItem softwareRepair = new JMenuItem("Software Repair");
        JMenuItem findEmployee = new JMenuItem("Search Employee");

        filterEmployeeByByJob.add(accountantCashier);//
        filterEmployeeByByJob.add(clientServiceExecutive);//
        filterEmployeeByByJob.add(frontDeskPersonnel);
        filterEmployeeByByJob.add(headTechnicalServices);
        filterEmployeeByByJob.add(inspector);
        filterEmployeeByByJob.add(maintenanceTech);
        filterEmployeeByByJob.add(phoneBuildTechnician);
        filterEmployeeByByJob.add(softwareRepair);

        filterAndSearch.add(filterEmployeeByByJob);
        filterAndSearch.add(findEmployee);

        viewEmployeeMenuBar.add(filterAndSearch);

        softwareRepair.addActionListener(e -> {
            employeeFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.filterEmployeeByJob("Software Repair"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        phoneBuildTechnician.addActionListener(e -> {
            employeeFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.filterEmployeeByJob("Phone-build technician"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        maintenanceTech.addActionListener(e -> {
            employeeFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.filterEmployeeByJob("Maintenance Tech"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        inspector.addActionListener(e -> {
            employeeFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.filterEmployeeByJob("Inspector"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        headTechnicalServices.addActionListener(e -> {
            employeeFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.filterEmployeeByJob("Head, Technical Services"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        frontDeskPersonnel.addActionListener(e -> {
            employeeFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.filterEmployeeByJob("Front Desk Personnel"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        accountantCashier.addActionListener(e -> {
            employeeFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.filterEmployeeByJob("Accountant/Cashier"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        clientServiceExecutive.addActionListener(e -> {
            employeeFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.filterEmployeeByJob("Client Service Executive"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        findEmployee.addActionListener( e -> {
            employeeFrame.dispose();
            try {
                printEmployeeTable(RepairShopData.findEmployee(JOptionPane.showInputDialog("Search employee:")));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        employeeFrame.add(scrollPane, BorderLayout.CENTER);
        employeeFrame.add(quitButton, BorderLayout.SOUTH);
        employeeFrame.add(viewEmployeeMenuBar, BorderLayout.NORTH);
        employeeFrame.setVisible(true);
    }

    /**
     * Method for printing inventory table
     */
    public static void printInventoryTable(ArrayList<Inventory> inventoryList) {
        JFrame inventoryFrame;
        JButton quitButton;
        JTable inventoryTable;

        String[] headers = new String[]{"Part ID", "Part Name", "Part cost", "Part Sell Price", "Quantity"};
        String[][] twoDimensionalArray = new String[inventoryList.size()][5];

        for (int i = 0; i < inventoryList.size(); i++) {
            Inventory inventory = inventoryList.get(i);
            twoDimensionalArray[i][0] = inventory.getPID();
            twoDimensionalArray[i][1] = inventory.getPName();
            twoDimensionalArray[i][2] = String.valueOf(inventory.getPCost());
            twoDimensionalArray[i][3] = String.valueOf(inventory.getPSellPrice());
            twoDimensionalArray[i][4] = String.valueOf(inventory.getQuantity());
        }

        inventoryTable = new JTable(twoDimensionalArray, headers);
        inventoryTable.setBounds(30, 40, 500, 400);
        inventoryTable.setDefaultEditor(Object.class, null);
        JScrollPane scrollPane = new JScrollPane(inventoryTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        quitButton = new JButton("Go back");
        quitButton.setBounds(30, 10, 150, 25);
        inventoryFrame = new JFrame("Inventory Table");
        inventoryFrame.setSize(600, 500);
        inventoryFrame.setLocationRelativeTo(null);
        inventoryFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quitButton.addActionListener(e -> {
            inventoryFrame.dispose();
            showTablesMenu();
        });

        //GUI for Filter/Search
        JMenuBar viewInventoryMenuBar = new JMenuBar();
        JMenu filterAndSearch = new JMenu("Filter / Search List");
        JMenu filterInventoryByPartName = new JMenu("Filter Inventory By Part Name");
        JMenuItem findInventory = new JMenuItem("Search Inventory");
        JMenuItem iPhone = new JMenuItem("Iphone");
        JMenuItem galaxy = new JMenuItem("Galaxy");
        JMenuItem tabA = new JMenuItem("Tab A");
        JMenuItem g5 = new JMenuItem("G5");
        JMenuItem note = new JMenuItem("Note");
        JMenuItem v12 = new JMenuItem("V12");
        JMenuItem pixel = new JMenuItem("Pixel");
        JMenuItem j7 = new JMenuItem("J7");
        JMenuItem five = new JMenuItem("Five");
        JMenuItem pro9 = new JMenuItem("9 Pro");

        filterInventoryByPartName.add(iPhone);
        filterInventoryByPartName.add(galaxy);
        filterInventoryByPartName.add(tabA);
        filterInventoryByPartName.add(g5);
        filterInventoryByPartName.add(note);
        filterInventoryByPartName.add(v12);
        filterInventoryByPartName.add(pixel);
        filterInventoryByPartName.add(j7);
        filterInventoryByPartName.add(five);
        filterInventoryByPartName.add(pro9);

        filterAndSearch.add(filterInventoryByPartName);
        filterAndSearch.add(findInventory);
        viewInventoryMenuBar.add(filterAndSearch);

        pro9.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("9 Pro"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        five.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("five"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        j7.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("j7"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        pixel.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("pixel"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        v12.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("v12"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        note.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("note"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        g5.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("g5"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        tabA.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("Tab A"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        galaxy.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("galaxy"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });


        iPhone.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.filterInventoryByPartName("Iphone"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        findInventory.addActionListener(e -> {
            inventoryFrame.dispose();
            try {
                printInventoryTable(RepairShopData.findInventory(JOptionPane.showInputDialog("Search inventory:")));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        //filterCustomerByAddress.addActionListener(RepairShopData.filterCustomerByAddress());

        inventoryFrame.add(scrollPane, BorderLayout.CENTER);
        inventoryFrame.add(quitButton, BorderLayout.SOUTH);
        inventoryFrame.add(viewInventoryMenuBar, BorderLayout.NORTH);
        inventoryFrame.setVisible(true);
    }

    /**
     * Method for printing payment table
     */
    public static void printPaymentTable(ArrayList<Payment> paymentList) {
        JFrame paymentFrame;
        JButton quitButton;
        JTable paymentTable;

        String[] headers = new String[]{"Payment ID", "Date Paid", "Amount Paid", "Payment Type", "Amount Remaining", "Request ID"};
        String[][] twoDimensionalArray = new String[paymentList.size()][6];

        for (int i = 0; i < paymentList.size(); i++) {
            Payment payment = paymentList.get(i);
            twoDimensionalArray[i][0] = payment.getPID();
            twoDimensionalArray[i][1] = payment.getDatePaid();
            twoDimensionalArray[i][2] = String.valueOf(payment.getAmountPaid());
            twoDimensionalArray[i][3] = payment.getPaymentType();
            twoDimensionalArray[i][4] = String.valueOf(payment.getAmountRemaining());
            twoDimensionalArray[i][5] = payment.getRequestID();
        }

        paymentTable = new JTable(twoDimensionalArray, headers);
        paymentTable.setBounds(30, 40, 500, 400);
        paymentTable.setDefaultEditor(Object.class, null);
        JScrollPane scrollPane = new JScrollPane(paymentTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        quitButton = new JButton("Go back");
        quitButton.setBounds(30, 10, 150, 25);
        paymentFrame = new JFrame("Payment Table");
        paymentFrame.setSize(600, 500);
        paymentFrame.setLocationRelativeTo(null);
        paymentFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quitButton.addActionListener(e -> {
            paymentFrame.dispose();
            showTablesMenu();
        });

        //GUI for Filter List
        JMenuBar viewPaymentMenuBar = new JMenuBar();
        JMenu filterList = new JMenu("Filter/Search List");
        JMenuItem findPayment = new JMenuItem("Search Payment");
        JMenu filterPaymentByTypeOfPayment = new JMenu("Filter Payment By Type Of Payment");
        JMenuItem filterPaymentByMonthAndYear = new JMenuItem("Filter Payment By Month And Year");
        JMenuItem bankTransfer = new JMenuItem("Bank Transfer");
        JMenuItem cash = new JMenuItem("Cash");
        JMenuItem eWallet = new JMenuItem("E-Wallet");
        JMenuItem credit = new JMenuItem("Credit");

        filterPaymentByTypeOfPayment.add(bankTransfer);
        filterPaymentByTypeOfPayment.add(cash);
        filterPaymentByTypeOfPayment.add(credit);
        filterPaymentByTypeOfPayment.add(eWallet);

        filterList.add(filterPaymentByTypeOfPayment);
        filterList.add(filterPaymentByMonthAndYear);
        filterList.add(findPayment);

        filterPaymentByMonthAndYear.addActionListener(e -> {
            paymentFrame.dispose();
            String month = showInputDialog("Enter the month (MM)");
            String year = showInputDialog("Enter the year (YYYY)");
            try {
                printPaymentTable(RepairShopData.filterPaymentByMonthAndYear(month, year));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        eWallet.addActionListener(e -> {
            paymentFrame.dispose();
            try {
                printPaymentTable(RepairShopData.filterPaymentByTypeOfPayment("E-wallet"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        credit.addActionListener(e -> {
            paymentFrame.dispose();
            try {
                printPaymentTable(RepairShopData.filterPaymentByTypeOfPayment("credit"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        cash.addActionListener(e -> {
            paymentFrame.dispose();
            try {
                printPaymentTable(RepairShopData.filterPaymentByTypeOfPayment("Cash"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        bankTransfer.addActionListener(e -> {
            paymentFrame.dispose();
            try {
                printPaymentTable(RepairShopData.filterPaymentByTypeOfPayment("Bank Transfer"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        viewPaymentMenuBar.add(filterList);
        findPayment.addActionListener(e -> {
           paymentFrame.dispose();
           try{
               printPaymentTable(RepairShopData.findPayment(JOptionPane.showInputDialog("Search Payment:")));
           } catch (Exception exception) {
               exception.printStackTrace();
           }
        });

        paymentFrame.add(scrollPane, BorderLayout.CENTER);
        paymentFrame.add(quitButton, BorderLayout.SOUTH);
        paymentFrame.add(viewPaymentMenuBar, BorderLayout.NORTH);
        paymentFrame.setVisible(true);

    }

    /**
     * Method for printing repairdetails table
     */
    public static void printRepairDetailsTable(ArrayList<RepairDetails> repairDetailsList) {
        JFrame repairDetailsFrame;
        JButton quitButton;
        JTable repairDetailsTable;

        String[] headers = new String[]{"Request ID", "Customer ID", "Employee ID", "Date Requested", "Phone Model",
                "Estimated Completion", "Total Amount", "Required Down Payment", "Status"};
        String[][] twoDimensionalArray = new String[repairDetailsList.size()][9];

        for (int i = 0; i < repairDetailsList.size(); i++) {
            RepairDetails repairDetails = repairDetailsList.get(i);
            twoDimensionalArray[i][0] = repairDetails.getRequestID();
            twoDimensionalArray[i][1] = repairDetails.getCID();
            twoDimensionalArray[i][2] = repairDetails.getEID();
            twoDimensionalArray[i][3] = repairDetails.getDateRequested();
            twoDimensionalArray[i][4] = repairDetails.getPhoneModel();
            twoDimensionalArray[i][5] = repairDetails.getEstimatedCompletion();
            twoDimensionalArray[i][6] = String.valueOf(repairDetails.getTotal());
            twoDimensionalArray[i][7] = String.valueOf(repairDetails.getRequiredDP());
            twoDimensionalArray[i][8] = repairDetails.getStatus();
        }

        repairDetailsTable = new JTable(twoDimensionalArray, headers);
        repairDetailsTable.setBounds(30, 40, 500, 400);
        repairDetailsTable.setDefaultEditor(Object.class, null);
        JScrollPane scrollPane = new JScrollPane(repairDetailsTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        quitButton = new JButton("Go back");
        quitButton.setBounds(30, 10, 150, 25);
        repairDetailsFrame = new JFrame("Repair Details Table");
        repairDetailsFrame.setSize(800, 500);
        repairDetailsFrame.setLocationRelativeTo(null);
        repairDetailsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quitButton.addActionListener(e -> {
            repairDetailsFrame.dispose();
            showTablesMenu();
        });

        JMenuBar viewRepairDetailsMenuBar = new JMenuBar();
        JMenu filterList = new JMenu("Filter/Search List");
        JMenuItem filterRepairDetailsByStatus = new JMenu("Filter Repair Details By Status");
        JMenuItem filterRepairDetailsByMonthAndYear = new JMenuItem("Filter Repair Details By Month And Year");
        JMenuItem pending = new JMenuItem("PENDING");
        JMenuItem ongoing = new JMenuItem("ONGOING");
        JMenuItem cancelled = new JMenuItem("CANCELLED");
        JMenuItem finished = new JMenuItem("FINISHED");
        JMenuItem returned = new JMenuItem("RETURNED");
        JMenuItem findRepairDetails = new JMenuItem("Search");

        filterRepairDetailsByStatus.add(pending);
        filterRepairDetailsByStatus.add(ongoing);
        filterRepairDetailsByStatus.add(cancelled);
        filterRepairDetailsByStatus.add(finished);
        filterRepairDetailsByStatus.add(returned);

        filterList.add(filterRepairDetailsByStatus);
        filterList.add(filterRepairDetailsByMonthAndYear);
        filterList.add(findRepairDetails);

        viewRepairDetailsMenuBar.add(filterList);

        filterRepairDetailsByMonthAndYear.addActionListener(e -> {
            repairDetailsFrame.dispose();
            String month = showInputDialog("Enter the month (MM)");
            String year = showInputDialog("Enter the year (YYYY)");
            try {
                printRepairDetailsTable(RepairShopData.filterRepairDetailsByMonthAndYear(month, year));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        returned.addActionListener(e -> {
            repairDetailsFrame.dispose();
            try {
                printRepairDetailsTable(RepairShopData.filterRepairDetailsByStatus("RETURNED"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        finished.addActionListener(e -> {
            repairDetailsFrame.dispose();
            try {
                printRepairDetailsTable(RepairShopData.filterRepairDetailsByStatus("FINISHED"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        cancelled.addActionListener(e -> {
            repairDetailsFrame.dispose();
            try {
                printRepairDetailsTable(RepairShopData.filterRepairDetailsByStatus("CANCELLED"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        ongoing.addActionListener(e -> {
            repairDetailsFrame.dispose();
            try {
                printRepairDetailsTable(RepairShopData.filterRepairDetailsByStatus("ONGOING"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        pending.addActionListener(e -> {
            repairDetailsFrame.dispose();
            try {
                printRepairDetailsTable(RepairShopData.filterRepairDetailsByStatus("PENDING"));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        findRepairDetails.addActionListener(e -> {
            repairDetailsFrame.dispose();
            try{
                printRepairDetailsTable(RepairShopData.findRepairDetails(JOptionPane.showInputDialog("Search repair details:")));
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });
        repairDetailsFrame.add(viewRepairDetailsMenuBar, BorderLayout.NORTH);

        repairDetailsFrame.add(scrollPane, BorderLayout.CENTER);
        repairDetailsFrame.add(quitButton, BorderLayout.SOUTH);
        repairDetailsFrame.setVisible(true);
    }

    /**
     * Method for printing repinv table
     */
    public static void printRepInvTable() throws Exception {
        JFrame repInvFrame;
        JButton quitButton;
        JTable repInvTable;

        ArrayList<RepInv> repInvList = RepairShopData.getRepInv();
        String[] headers = new String[]{"Request ID", "Part ID"};
        String[][] twoDimensionalArray = new String[repInvList.size()][2];

        for (int i = 0; i < repInvList.size(); i++) {
            RepInv repInv = repInvList.get(i);
            twoDimensionalArray[i][0] = repInv.getReqID();
            twoDimensionalArray[i][1] = repInv.getPartID();
        }

        repInvTable = new JTable(twoDimensionalArray, headers);
        repInvTable.setBounds(30, 40, 500, 400);
        repInvTable.setDefaultEditor(Object.class, null);
        JScrollPane scrollPane = new JScrollPane(repInvTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        quitButton = new JButton("Go back");
        quitButton.setBounds(30, 10, 150, 25);
        repInvFrame = new JFrame("RepInv Table");
        repInvFrame.setSize(400, 500);
        repInvFrame.setLocationRelativeTo(null);
        repInvFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quitButton.addActionListener(e -> {
            repInvFrame.dispose();
            showTablesMenu();
        });
        repInvFrame.add(scrollPane, BorderLayout.CENTER);
        repInvFrame.add(quitButton, BorderLayout.SOUTH);
        repInvFrame.setVisible(true);

    }

    public static void printRepairTypeTable() throws Exception {
        JFrame repTypeFrame;
        JButton quitButton;
        JTable repTypeTable;

        ArrayList<RepairType> repairTypeList = RepairShopData.getRepairTypes();
        String[] headers = new String[]{"Type ID", "Damage Info", "Service Price"};
        String[][] twoDimensionalArray = new String[repairTypeList.size()][3];

        for (int i = 0; i < repairTypeList.size(); i++) {
            RepairType repairType = repairTypeList.get(i);
            twoDimensionalArray[i][0] = repairType.getTypeID();
            twoDimensionalArray[i][1] = repairType.getDamageInfo();
            twoDimensionalArray[i][2] = String.valueOf(repairType.getServicePrice());
        }

        repTypeTable = new JTable(twoDimensionalArray, headers);
        repTypeTable.setBounds(30, 40, 500, 400);
        repTypeTable.setDefaultEditor(Object.class, null);
        JScrollPane scrollPane = new JScrollPane(repTypeTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        quitButton = new JButton("Go back");
        quitButton.setBounds(30, 10, 150, 25);
        repTypeFrame = new JFrame("Repair Type Table");
        repTypeFrame.setSize(500, 500);
        repTypeFrame.setLocationRelativeTo(null);
        repTypeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quitButton.addActionListener(e -> {
            repTypeFrame.dispose();
            showTablesMenu();
        });
        repTypeFrame.add(scrollPane, BorderLayout.CENTER);
        repTypeFrame.add(quitButton, BorderLayout.SOUTH);
        repTypeFrame.setVisible(true);
    }

    /**
     * Method for printing typerep table
     */
    public static void printTypeRepTable() throws Exception {
        JFrame typeRepFrame;
        JButton quitButton;
        JTable typeRepTable;

        ArrayList<TypeRep> typeRepList = RepairShopData.getTypeRep();
        String[] headers = new String[]{"Request ID", "Type ID"};
        String[][] twoDimensionalArray = new String[typeRepList.size()][2];

        for (int i = 0; i < typeRepList.size(); i++) {
            TypeRep typeRep = typeRepList.get(i);
            twoDimensionalArray[i][0] = typeRep.getReqID();
            twoDimensionalArray[i][1] = typeRep.getTypeID();
        }

        typeRepTable = new JTable(twoDimensionalArray, headers);
        typeRepTable.setBounds(30, 40, 500, 400);
        typeRepTable.setDefaultEditor(Object.class, null);
        JScrollPane scrollPane = new JScrollPane(typeRepTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        quitButton = new JButton("Go Back");
        quitButton.setBounds(30, 10, 150, 25);
        typeRepFrame = new JFrame("RepInv Table");
        typeRepFrame.setSize(400, 500);
        typeRepFrame.setLocationRelativeTo(null);
        typeRepFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        quitButton.addActionListener(e -> {
            typeRepFrame.dispose();
            showTablesMenu();
        });
        typeRepFrame.add(scrollPane, BorderLayout.CENTER);
        typeRepFrame.add(quitButton, BorderLayout.SOUTH);
        typeRepFrame.setVisible(true);

    }

    /**
     * Method for adding customer
     */
    public static void addCustomer() {
        JFrame addCustomerFrame = new JFrame("Add Employee");
        JPanel addCustomerPanel = new JPanel();
        addCustomerFrame.getContentPane().setLayout(new BorderLayout());
        addCustomerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addCustomerFrame.setResizable(false);
        addCustomerFrame.setSize(300, 420);
        addCustomerFrame.setLocationRelativeTo(null);
        addCustomerFrame.add(addCustomerPanel);
        addCustomerPanel.setLayout(null);

        JLabel header = new JLabel("Enter customer details");
        header.setBounds(45, 10, 300, 25);
        header.setFont(new Font("Dialog", Font.BOLD, 15));
        addCustomerPanel.add(header);


        JLabel customerName = new JLabel("Customer Name:");
        customerName.setBounds(15, 50, 100, 25);
        addCustomerPanel.add(customerName);

        JTextField customerNameField = new JTextField(20);
        customerNameField.setBounds(120, 50, 135, 25);
        addCustomerPanel.add(customerNameField);

        JLabel customerNumber = new JLabel("Mobile Number:");
        customerNumber.setBounds(15, 100, 100, 25);
        addCustomerPanel.add(customerNumber);

        JTextField customerNumberField = new JTextField(20);
        customerNumberField.setBounds(120, 100, 135, 25);
        addCustomerPanel.add(customerNumberField);

        JLabel customerEmail = new JLabel("Email:");
        customerEmail.setBounds(15, 150, 150, 25);
        addCustomerPanel.add(customerEmail);

        JTextField customerEmailField = new JTextField(20);
        customerEmailField.setBounds(120, 150, 135, 25);
        addCustomerPanel.add(customerEmailField);

        JLabel customerAddress = new JLabel("Address:");
        customerAddress.setBounds(15, 200, 100, 25);
        addCustomerPanel.add(customerAddress);

        JTextField customerAddressField = new JTextField(20);
        customerAddressField.setBounds(120, 200, 135, 25);
        addCustomerPanel.add(customerAddressField);

        JButton createItemButton = new JButton("Add");
        createItemButton.setBounds(90, 250, 100, 25);
        createItemButton.addActionListener(e -> {
            addCustomerFrame.dispose();
            RepairShopData.addCustomer(new Customer("0", customerNameField.getText(),
                    customerNumberField.getText(), customerEmailField.getText(), customerAddressField.getText()));
            JOptionPane.showMessageDialog(null, "The customer is added!");
            addOperations();
        });
        addCustomerPanel.add(createItemButton);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.setBounds(90, 285, 100, 25);
        cancelButton.addActionListener(e -> {
            addCustomerFrame.dispose();
            addOperations();
        });
        addCustomerPanel.add(cancelButton);

        addCustomerFrame.setVisible(true);
    }

    /**
     * Method for adding employee
     */
    public static void addEmployee() {
        JFrame addEmployeeFrame = new JFrame("Create item");
        JPanel addEmployeePanel = new JPanel();
        addEmployeeFrame.getContentPane().setLayout(new BorderLayout());
        addEmployeeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addEmployeeFrame.setResizable(false);
        addEmployeeFrame.setSize(300, 420);
        addEmployeeFrame.setLocationRelativeTo(null);
        addEmployeeFrame.add(addEmployeePanel);
        addEmployeePanel.setLayout(null);

        JLabel header = new JLabel("Enter employee details");
        header.setBounds(45, 10, 300, 25);
        header.setFont(new Font("Dialog", Font.BOLD, 15));
        addEmployeePanel.add(header);

        JLabel employeeID = new JLabel("Employee ID:");
        employeeID.setBounds(15, 50, 100, 25);
        addEmployeePanel.add(employeeID);

        JTextField employeeIDField = new JTextField(20);
        employeeIDField.setBounds(120, 50, 135, 25);
        addEmployeePanel.add(employeeIDField);

        JLabel employeeName = new JLabel("Employee Name:");
        employeeName.setBounds(15, 100, 100, 25);
        addEmployeePanel.add(employeeName);

        JTextField employeeNameField = new JTextField(20);
        employeeNameField.setBounds(120, 100, 135, 25);
        addEmployeePanel.add(employeeNameField);

        JLabel employeeNumber = new JLabel("Mobile Number:");
        employeeNumber.setBounds(15, 150, 100, 25);
        addEmployeePanel.add(employeeNumber);

        JTextField employeeNumberField = new JTextField(20);
        employeeNumberField.setBounds(120, 150, 135, 25);
        addEmployeePanel.add(employeeNumberField);

        JLabel employeeEmail = new JLabel("Email:");
        employeeEmail.setBounds(15, 200, 100, 25);
        addEmployeePanel.add(employeeEmail);

        JTextField employeeEmailField = new JTextField(20);
        employeeEmailField.setBounds(120, 200, 135, 25);
        addEmployeePanel.add(employeeEmailField);

        JLabel employeeAddress = new JLabel("Address:");
        employeeAddress.setBounds(15, 250, 100, 25);
        addEmployeePanel.add(employeeAddress);

        JTextField employeeAddressField = new JTextField(20);
        employeeAddressField.setBounds(120, 250, 135, 25);
        addEmployeePanel.add(employeeAddressField);

        JButton createItemButton = new JButton("Add");
        createItemButton.setBounds(90, 300, 100, 25);
        createItemButton.addActionListener(e -> {
            addEmployeeFrame.dispose();
            RepairShopData.addEmployee(new Employee(employeeIDField.getText(), employeeNameField.getText(),
                    employeeNumberField.getText(), employeeEmailField.getText(), employeeAddressField.getText()));
            JOptionPane.showMessageDialog(null, "The employee is added!");
            addOperations();
        });
        addEmployeePanel.add(createItemButton);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.setBounds(90, 335, 100, 25);
        cancelButton.addActionListener(e -> {
            addEmployeeFrame.dispose();
            addOperations();
        });
        addEmployeePanel.add(cancelButton);

        addEmployeeFrame.setVisible(true);
    }

    /**
     * Method for adding repairdetails
     */
    public static void addRepairDetails() throws Exception {
        JFrame addRepairDetails = new JFrame("Add Repair Details");
        JPanel addRepairDetailsPanel = new JPanel();
        addRepairDetails.getContentPane().setLayout(new BorderLayout());
        addRepairDetails.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addRepairDetails.setResizable(false);
        addRepairDetails.setSize(330, 470);
        addRepairDetails.setLocationRelativeTo(null);
        addRepairDetails.add(addRepairDetailsPanel);
        addRepairDetailsPanel.setLayout(null);

        JLabel header = new JLabel("Enter repair details");
        header.setBounds(45, 10, 300, 25);
        header.setFont(new Font("Dialog", Font.BOLD, 15));
        addRepairDetailsPanel.add(header);

        JLabel cID = new JLabel("Customer ID");
        cID.setBounds(15, 50, 100, 25);
        addRepairDetailsPanel.add(cID);

        String[] test = RepairShopData.getCustomerNameList();
        JComboBox<String> cIDBox = new JComboBox<>(test);
        cIDBox.setBounds(165, 50, 135, 25);
        addRepairDetailsPanel.add(cIDBox);

        JLabel eID = new JLabel("Employee ID:");
        eID.setBounds(15, 100, 100, 25);
        addRepairDetailsPanel.add(eID);

        String[] test2 = RepairShopData.getEmployeeNameList();
        JComboBox<String> eIDBox = new JComboBox<>(test2);
        eIDBox.setBounds(165, 100, 135, 25);
        addRepairDetailsPanel.add(eIDBox);

        JLabel dateRequested = new JLabel("Date Requested:");
        dateRequested.setBounds(15, 150, 100, 25);
        addRepairDetailsPanel.add(dateRequested);

        JTextField dateRequestedField = new JTextField(20);
        dateRequestedField.setBounds(165, 150, 135, 25);
        addRepairDetailsPanel.add(dateRequestedField);

        JLabel phoneModel = new JLabel("Phone Model:");
        phoneModel.setBounds(15, 200, 100, 25);
        addRepairDetailsPanel.add(phoneModel);

        JTextField phoneModelField = new JTextField(20);
        phoneModelField.setBounds(165, 200, 135, 25);
        addRepairDetailsPanel.add(phoneModelField);

        JLabel estimatedCompletion = new JLabel("Estimated Completion:");
        estimatedCompletion.setBounds(15, 250, 150, 25);
        addRepairDetailsPanel.add(estimatedCompletion);

        JTextField estimatedCompletionField = new JTextField(20);
        estimatedCompletionField.setBounds(165, 250, 135, 25);
        addRepairDetailsPanel.add(estimatedCompletionField);

        JLabel status = new JLabel("Status:");
        status.setBounds(15, 300, 100, 25);
        addRepairDetailsPanel.add(status);

        String[] test3 = new String[]{"PENDING", "ONGOING", "FINISHED", "RETURNED", "CANCELLED"};
        JComboBox<String> statusBox = new JComboBox<>(test3);
        statusBox.setBounds(165, 300, 135, 25);
        addRepairDetailsPanel.add(statusBox);

        JButton createItemButton = new JButton("Add");
        createItemButton.setBounds(110, 350, 100, 25);
        createItemButton.addActionListener(e -> {
            addRepairDetails.dispose();
            RepairShopData.addRepairDetails(new RepairDetails("",
                   RepairShopData.getCID(Objects.requireNonNull(cIDBox.getSelectedItem()).toString())
                    ,RepairShopData.getEID(Objects.requireNonNull(eIDBox.getSelectedItem()).toString()),
                    dateRequestedField.getText(), phoneModelField.getText(), estimatedCompletionField.getText(),
                    0, 0, (String) statusBox.getSelectedItem()));
            try {
                addTypeRep();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        addRepairDetailsPanel.add(createItemButton);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.setBounds(110, 385, 100, 25);
        cancelButton.addActionListener(e -> {
            addRepairDetails.dispose();
            addOperations();
        });
        addRepairDetailsPanel.add(cancelButton);

        addRepairDetails.setVisible(true);
    }

    public static void addTypeRep() throws Exception {
        JFrame addTypeRep = new JFrame("Add Type of Repair");
        JPanel addTypeRepPanel = new JPanel();
        addTypeRep.getContentPane().setLayout(new BorderLayout());
        addTypeRep.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addTypeRep.setResizable(false);
        addTypeRep.setSize(330, 250);
        addTypeRep.setLocationRelativeTo(null);
        addTypeRep.add(addTypeRepPanel);
        addTypeRepPanel.setLayout(null);

        JLabel header = new JLabel("TypeRep Relationship");
        header.setBounds(45, 10, 300, 25);
        header.setFont(new Font("Dialog", Font.BOLD, 15));
        addTypeRepPanel.add(header);

        JLabel cID = new JLabel("Request ID");
        cID.setBounds(15, 50, 100, 25);
        addTypeRepPanel.add(cID);

        JLabel requestIDLabel = new JLabel(RepairShopData.getRID());
        requestIDLabel.setBounds(165, 50, 135, 25);
        addTypeRepPanel.add(requestIDLabel);

        JLabel typeOfRepair = new JLabel("Type of Repair");
        typeOfRepair.setBounds(15, 100, 100, 25);
        addTypeRepPanel.add(typeOfRepair);

        String[] test2 = RepairShopData.getTypeOfRepairList();
        JComboBox<String> typeRepairBox = new JComboBox<>(test2);
        typeRepairBox.setBounds(165, 100, 135, 25);
        addTypeRepPanel.add(typeRepairBox);

        JButton createItemButton = new JButton("Confirm");
        createItemButton.setBounds(110, 150, 100, 25);
        createItemButton.addActionListener(e -> {
            addTypeRep.dispose();
            RepairShopData.addTypeRep(requestIDLabel.getText(), RepairShopData.getTypeRepID((String) typeRepairBox.getSelectedItem()));
            int result = JOptionPane.showConfirmDialog(null, "Do you need to add more?", "Add more?", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                try {
                    addTypeRep();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (result == JOptionPane.NO_OPTION){
                try {
                    addRepInv();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        addTypeRepPanel.add(createItemButton);

        addTypeRep.setVisible(true);
    }

    public static void addRepInv() throws Exception {
        JFrame addRepInv = new JFrame("Add Repair Inventory");
        JPanel addRepInvPanel = new JPanel();
        addRepInv.getContentPane().setLayout(new BorderLayout());
        addRepInv.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addRepInv.setResizable(false);
        addRepInv.setSize(330, 250);
        addRepInv.setLocationRelativeTo(null);
        addRepInv.add(addRepInvPanel);
        addRepInvPanel.setLayout(null);

        JLabel header = new JLabel("TypeRep Relationship");
        header.setBounds(45, 10, 300, 25);
        header.setFont(new Font("Dialog", Font.BOLD, 15));
        addRepInvPanel.add(header);

        JLabel cID = new JLabel("Request ID");
        cID.setBounds(15, 50, 100, 25);
        addRepInvPanel.add(cID);

        JLabel requestIDLabel = new JLabel(RepairShopData.getRID());
        requestIDLabel.setBounds(165, 50, 135, 25);
        addRepInvPanel.add(requestIDLabel);

        JLabel typeOfRepair = new JLabel("Employee ID:");
        typeOfRepair.setBounds(15, 100, 100, 25);
        addRepInvPanel.add(typeOfRepair);

        String[] test2 = RepairShopData.getInventoryList();
        JComboBox<String> invBox = new JComboBox<>(test2);
        invBox.setBounds(165, 100, 135, 25);
        addRepInvPanel.add(invBox);

        JButton createItemButton = new JButton("Confirm");
        createItemButton.setBounds(110, 150, 100, 25);
        createItemButton.addActionListener(e -> {
            addRepInv.dispose();
            RepairShopData.addRepInv(requestIDLabel.getText(), RepairShopData.getRepInvID((String) invBox.getSelectedItem()));
            RepairShopData.decrementInventoryItem(RepairShopData.getRepInvID((String) invBox.getSelectedItem()));
            int result = JOptionPane.showConfirmDialog(null, "Do you need to add more?", "Add more?", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                try {
                    addRepInv();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (result == JOptionPane.NO_OPTION){
                RepairShopData.updateRepairDetails(requestIDLabel.getText());
                JOptionPane.showMessageDialog(null,"Successfully added!");
                addOperations();
            }
        });
        addRepInvPanel.add(createItemButton);

        addRepInv.setVisible(true);
    }

    /**
     * Method for adding inventory
     */
    public static void addInventory(){
        JFrame addInventoryFrame = new JFrame("Add Inventory");
        JPanel addInventoryPanel = new JPanel();
        addInventoryFrame.getContentPane().setLayout(new BorderLayout());
        addInventoryFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addInventoryFrame.setResizable(false);
        addInventoryFrame.setSize(300, 420);
        addInventoryFrame.setLocationRelativeTo(null);
        addInventoryFrame.add(addInventoryPanel);
        addInventoryPanel.setLayout(null);

        JLabel header = new JLabel("Enter inventory details");
        header.setBounds(45, 10, 300, 25);
        header.setFont(new Font("Dialog", Font.BOLD, 15));
        addInventoryPanel.add(header);

        JLabel partID = new JLabel("Part ID:");
        partID.setBounds(15, 50, 100, 25);
        addInventoryPanel.add(partID);

        JTextField partIDField = new JTextField(20);
        partIDField.setBounds(120, 50, 135, 25);
        addInventoryPanel.add(partIDField);

        JLabel partName = new JLabel("Part Name:");
        partName.setBounds(15, 100, 100, 25);
        addInventoryPanel.add(partName);

        JTextField partNameField = new JTextField(20);
        partNameField.setBounds(120, 100, 135, 25);
        addInventoryPanel.add(partNameField);

        JLabel partCose = new JLabel("Part Cost:");
        partCose.setBounds(15, 150, 100, 25);
        addInventoryPanel.add(partCose);

        JTextField partCostField = new JTextField(20);
        partCostField.setBounds(120, 150, 135, 25);
        addInventoryPanel.add(partCostField);

        JLabel partSellPrice = new JLabel("Selling Price:");
        partSellPrice.setBounds(15, 200, 100, 25);
        addInventoryPanel.add(partSellPrice);

        JTextField partSellPriceField = new JTextField(20);
        partSellPriceField.setBounds(120, 200, 135, 25);
        addInventoryPanel.add(partSellPriceField);

        JLabel partQuantity = new JLabel("Quantity:");
        partQuantity.setBounds(15, 250, 100, 25);
        addInventoryPanel.add(partQuantity);

        JTextField quantityField = new JTextField(20);
        quantityField.setBounds(120, 250, 135, 25);
        addInventoryPanel.add(quantityField);

        JButton createItemButton = new JButton("Add");
        createItemButton.setBounds(90, 300, 100, 25);
        createItemButton.addActionListener(e -> {
            addInventoryFrame.dispose();
            RepairShopData.addInventory(new Inventory(partIDField.getText(), partNameField.getText(), Double.parseDouble(partCostField.getText()),
                    Double.parseDouble(partSellPriceField.getText()), Integer.parseInt(quantityField.getText())));
            JOptionPane.showMessageDialog(null, "The inventory is added!");
            addOperations();
        });
        addInventoryPanel.add(createItemButton);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.setBounds(90, 335, 100, 25);
        cancelButton.addActionListener(e -> {
            addInventoryFrame.dispose();
            addOperations();
        });
        addInventoryPanel.add(cancelButton);

        addInventoryFrame.setVisible(true);
    }

    /**
     * Method to show input dialog
     */
    public static String showInputDialog(String s) {
        return JOptionPane.showInputDialog(s);
    }
}
