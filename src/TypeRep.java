public class TypeRep {
    private String reqID;
    private String typeID;

    // Constructors
    public TypeRep() {

    }

    public TypeRep(String repID, String typeID) {
        this.reqID = repID;
        this.typeID = typeID;
    }

    // Getters
    public String getReqID() {
        return reqID;
    }

    public String getTypeID() {
        return typeID;
    }

    // Setters
    public void setReqID(String reqID) {
        this.reqID = reqID;
    }

    public void setTypeID(String typeID) {
        this.typeID = typeID;
    }

    // toString method
    public String toString() {
        return reqID +", "+ typeID;
    }
}
