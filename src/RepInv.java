public class RepInv {
    private String reqID;
    private String partID;

    // Constructors
    public RepInv() {

    }

    public RepInv(String repID, String partID) {
        this.reqID = repID;
        this.partID = partID;
    }

    // Getters
    public String getReqID() {
        return reqID;
    }

    public String getPartID() {
        return partID;
    }

    // Setters
    public void setReqID(String reqID) {
        this.reqID = reqID;
    }

    public void setPartID(String partID) {
        this.partID = partID;
    }

    // toString method
    public String toString() {
        return reqID +", "+ partID;
    }
}
