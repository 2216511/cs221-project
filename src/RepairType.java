public class RepairType {
    private String typeID;
    private String damageInfo;
    private double servicePrice;

    // Constructors
    public RepairType() {

    }

    public RepairType(String typeID, String damageInfo, double servicePrice) {
        this.typeID = typeID;
        this.damageInfo = damageInfo;
        this.servicePrice = servicePrice;
    }

    // Getters
    public String getTypeID() {
        return typeID;
    }

    public String getDamageInfo() {
        return damageInfo;
    }

    public double getServicePrice() {
        return servicePrice;
    }

    // Setters
    public void setTypeID(String typeID) {
        this.typeID = typeID;
    }

    public void setDamageInfo(String damageInfo) {
        this.damageInfo = damageInfo;
    }

    public void setServicePrice(double servicePrice) {
        this.servicePrice = servicePrice;
    }

    // toString method
    public String toString() {
        return typeID +", "+ damageInfo +", "+ servicePrice;
    }
}
