public class Inventory {
    private String partID;
    private String partName;
    private double partCost;
    private double partSellPrice;
    private int quantity;

    // Constructors
    public Inventory() {

    }

    public Inventory(String id, String name, double cost, double sellPrice, int quantity) {
        this.partID = id;
        this.partName = name;
        this.partCost = cost;
        this.partSellPrice = sellPrice;
        this.quantity = quantity;
    }

    // Getters
    public String getPID() {
        return partID;
    }

    public String getPName() {
        return partName;
    }

    public double getPCost() {
        return partCost;
    }

    public double getPSellPrice() {
        return partSellPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    // Setters
    public void setPID(String id) {
        this.partID = partID;
    }

    public void setPName(String n) {
        this.partName = n;
    }

    public void setPCost(double cost) {
        this.partCost = cost;
    }

    public void setPSellPrice(double sp) {
        this.partSellPrice = sp;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    // toString method
    public String toString() {
        return partID +", "+ partName +", "+ partCost +", "+ ", "+ partSellPrice +", "+ quantity;
    }
}
