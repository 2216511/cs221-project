public class Payment {
    private String PID;
    private String datePaid;
    private double amountPaid;
    private String paymentType;
    private double amountRemaining;
    private String requestID;

    // Constructors
    public Payment() {

    }

    public Payment(String id, String datePaid, double amountPaid, String paymentType, double amountRemaining, String reqID) {
        this.PID = id;
        this.datePaid = datePaid;
        this.amountPaid = amountPaid;
        this.paymentType = paymentType;
        this.amountRemaining = amountRemaining;
        this.requestID = reqID;
    }

    // Getters
    public String getPID() {
        return PID;
    }

    public String getDatePaid() {
        return datePaid;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public double getAmountRemaining() {
        return amountRemaining;
    }

    public String getRequestID() {
        return requestID;
    }

    // Setters
    public void setPID(String id) {
        this.PID = id;
    }

    public void setDatePaid(String dp) {
        this.datePaid = dp;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public void setPaymentType(String pt) {
        this.paymentType = pt;
    }

    public void setAmountRemaining(double amountRemaining) {
        this.amountRemaining = amountRemaining;
    }

    public void setRequestID(String reqID) {
        this.requestID = reqID;
    }

    // toString method
    public String toString() {
        return PID +", "+ datePaid +", "+ amountPaid +", "+ ", "+ paymentType +", "+ amountRemaining +", " + requestID;
    }
}
