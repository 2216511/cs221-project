public class RepairDetails {
    private String requestID;
    private String cID;
    private String eID;
    private String dateRequested;
    private String phoneModel;
    private String estimatedCompletion;
    private double totalAmount;
    private double requiredDP;
    private String status;

    // Constructors
    public RepairDetails() {

    }

    public RepairDetails(String cid, String eid, String dateRequested, String pModel, String estimatedCompletion) {
        this.requestID = "";
        this.cID = cid;
        this.eID = eid;
        this.dateRequested = dateRequested;
        this.phoneModel = pModel;
        this.estimatedCompletion = estimatedCompletion;
        this.totalAmount = 0.0;
        this.requiredDP = 0.0;
        this.status = "PENDING";
    }

    public RepairDetails(String rID, String cID, String eID, String dateRequested, String pModel, String estimatedCompletion, double totalAmount, double downPayment, String status) {
        this.requestID = rID;
        this.cID = cID;
        this.eID = eID;
        this.dateRequested = dateRequested;
        this.phoneModel = pModel;
        this.estimatedCompletion = estimatedCompletion;
        this.totalAmount = totalAmount;
        this.requiredDP = downPayment;
        this.status = status;
    }

    // Getters
    public String getRequestID() {
        return requestID;
    }

    public String getCID() {
        return cID;
    }

    public String getEID() {
        return eID;
    }

    public String getDateRequested() {
        return dateRequested;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public String getEstimatedCompletion() {
        return estimatedCompletion;
    }

    public double getTotal() {
        return totalAmount;
    }

    public double getRequiredDP() {
        return requiredDP;
    }

    public String getStatus() {
        return status;
    }

    // Setters
    public void setRequestID(String rid) {
        this.requestID = rid;
    }

    public void setCID(String cid) {
        this.cID = cid;
    }

    public void setEID(String eid) {
        this.eID = eid;
    }

    public void setDateRequested(String dateRequested) {
        this.dateRequested = dateRequested;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public void setEstimatedCompletion(String estimatedCompletion) {
        this.estimatedCompletion = estimatedCompletion;
    }

    public void setTotalAmount(double amount) {
        this.totalAmount = amount;
    }

    public void setRequiredDP(double dpAmount) {
        this.requiredDP = dpAmount;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // toString method
    public String toString() {
        return requestID +", "+ cID +","+ eID +", "+ dateRequested +", "+ phoneModel +", "+ ", "+ estimatedCompletion +", "+ totalAmount +", "+ requiredDP +", "+ status;
    }
}
