public class Customer {
    private String cID;
    private String cName;
    private String cMobNum;
    private String cEmail;
    private String cAddress;

    // Constructors
    public Customer() {

    }

    public Customer(String name, String mobileNum, String email, String address) {
        this.cID = "";
        this.cName = name;
        this.cMobNum = mobileNum;
        this.cEmail = email;
        this.cAddress = address;
    }

    public Customer(String id, String name, String mobileNum, String email, String address) {
        this.cID = id;
        this.cName = name;
        this.cMobNum = mobileNum;
        this.cEmail = email;
        this.cAddress = address;
    }

    // Getters
    public String getCID() {
        return cID;
    }

    public String getCName() {
        return cName;
    }

    public String getCMobNum() {
        return cMobNum;
    }

    public String getCEmail() {
        return cEmail;
    }

    public String getCAddress() {
        return cAddress;
    }

    // Setters
    public void setCID(String id) {
        this.cID = cID;
    }

    public void setCName(String n) {
        this.cName = n;
    }

    public void setCMobNum(String mNum) {
        this.cMobNum = mNum;
    }

    public void setCEmail(String email) {
        this.cEmail = email;
    }

    public void setCAddress(String address) {
        this.cAddress = address;
    }

    // toString method
    public String toString() {
        return cID +", "+ cName +", "+ cMobNum +", "+ ", "+ cEmail +", "+ cAddress;
    }
}
