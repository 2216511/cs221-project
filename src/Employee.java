public class Employee {
    private String eID;
    private String eName;
    private String eMobNum;
    private String eEmail;
    private String job;

    // Constructors
    public Employee() {

    }

    public Employee(String id, String name, String mobileNum, String email, String job) {
        this.eID = id;
        this.eName = name;
        this.eMobNum = mobileNum;
        this.eEmail = email;
        this.job = job;
    }

    // Getters
    public String getEID() {
        return eID;
    }

    public String getEName() {
        return eName;
    }

    public String getEMobNum() {
        return eMobNum;
    }

    public String getEEmail() {
        return eEmail;
    }

    public String getEJob() {
        return job;
    }

    // Setters
    public void setEID(String id) {
        this.eID = eID;
    }

    public void setEName(String n) {
        this.eName = n;
    }

    public void setEMobNum(String mNum) {
        this.eMobNum = mNum;
    }

    public void setEEmail(String email) {
        this.eEmail = email;
    }

    public void setJob(String address) {
        this.job = address;
    }

    // toString method
    public String toString() {
        return eID +", "+ eName +", "+ eMobNum +", "+ ", "+ eEmail +", "+ job;
    }
}
